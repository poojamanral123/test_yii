<?php
class AreAnagrams
{
    public static function areStringsAnagrams($a, $b)
    {

   	$a = str_split($a);

    $test = array();
    $compare = array();

    foreach ($a as $key) {
        if (!in_array($key, $test)) {
            array_push($test, $key);
            $compare[$key] = 1;
        } else {
            $compare[$key] += 1;
        } 
    }

    foreach ($compare as $key => $value) {
        if ($value !== substr_count($b, $key)) {
            return false;
        }

    }
    return true;
	}
}

echo AreAnagrams::areStringsAnagrams('momdad', 'dadmom') ? 'True' : 'False';// class AreAnagrams
	//echo $words;
?>
