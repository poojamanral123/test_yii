<?php

namespace \components;

use Yii;
use common\models\Users;
use common\models\AuthItem;
use yii\helpers\ArrayHelper;

/**
 * 
 * This is the model class for table "wb_color_scheme".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property integer $sort_order
 * @property string $created_at
 * @property integer $created_by
 * @property string $modified_at
 * @property integer $modified_by
 *
 * @property Users $modifiedBy
 * @property Users $createdBy
 */
class CommonUtility extends \yii\db\ActiveRecord {

    public static function getUserList() {
        $Users = Users::find()->where(['status' => 2])->AsArray()->all();
        return ArrayHelper::map($Users, 'id', 'username');
    }

    /*
     * get a Roles list for type = 1 & get Items list for type 2, for both pass [1,2] 
     */

    public static function getItems($type = "0", $alldetails = false) {
        $Roles = AuthItem::find()->where(['IN', 'type', $type])->AsArray()->all();
        if ($alldetails) {
            
        } else {
            $Roles = ArrayHelper::map($Roles, 'name', 'name');
        }
        return $Roles;
    }

    public static function getItemsByRoles($role_name = '') {
        $auth_item = AuthItem::find()->where(['name' => $role_name])->one();
        
        $child_items = [];
        if (!empty($auth_item->children)) {
            foreach ($auth_item->children as $value) {
                $child_items[] = $value->name;
            }
        }
        return $child_items;
    }

}
