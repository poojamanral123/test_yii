<?php

use yii\db\Migration;

/**
 * Handles the creation of table `wb_users`.
 */
class m161205_061619_create_wb_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
         //   'id' => $this->integer(),
            'first_name' => $this->string(30),
            'last_name' => $this->string(30),
            'username' => $this->string(15),
            'auth_key' => $this->string(32),
            'email' => $this->string(50),
            'password' => $this->string(60),
            'password_reset_token' => $this->string(100),
            'status' => $this->integer(),
            'Type' => $this->string(50),
            'created_at' => $this->datetime(),
            'created_by' => $this->integer(),
            'modified_at' => $this->timestamp(),
            'modified_by' => $this->integer(),
        ]);

         $this->alterColumn('{{%users}}', 'id', $this->smallInteger(8).' NOT NULL AUTO_INCREMENT');


         $this->batchInsert('{{%users}}', ['first_name', 'last_name','username','email','password','status','Type'], [
                                          [ 'Pooja', 'Manral','pooja.manral','pooja.manral@neosofttech.com','$2y$13$P5oQH0KL9fXs2F4H34Iw/.yl1yfFzLy8pW951dXQQG.ImQBkvxaFu',2,'Admin'],
                                          [ 'aaa' ,'bbb' ,'aaa.bbb','aabbb@gmail.com','$2y$13$ujxlKPdr6IHNO1FSj68NPOYllbk53JwynUPQfiLJ4FBw7abbBdGYC',2,'Frontend'],
                                          ['Super', 'admin', 'super.admin', 'super@gmail.com', '$2y$13$ACM3/Zh9jrn2Y4H0SDyu1OGwmvd.1b6kuJxjIQ4DNAt1QGxA/u8.y', 2, 'Super Admin'],
                                          ]);
     }


    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%users}}');
    }
}
