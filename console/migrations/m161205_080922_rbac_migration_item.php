<?php

use yii\db\Migration;

class m161205_080922_rbac_migration_item extends Migration
{
    public function up()
    {
       $this->batchInsert('wb_auth_item', ['name', 'type', 'description', 'rule_name', 'data', 'created_at', 'updated_at'],
                                            [['admin', 1, 'Admin can perform backend actions', NULL, '', NULL, NULL],
                                            ['backend.authassignment.create', 2, 'backend.authassignment.create', NULL, '', NULL, NULL],
                                            ['backend.authassignment.delete', 2, 'sdasdas', NULL, '', NULL, NULL],
                                            ['backend.authassignment.index', 2, 'backend.authassignment.index', NULL, NULL, NULL, NULL],
                                            ['backend.authassignment.update', 2, 'sfsdf', NULL, '', NULL, NULL],
                                            ['backend.authitem.assign', 2, 'dasdsa', NULL, '', NULL, NULL],
                                            ['backend.authitem.create', 2, 'backend.authitem.create', NULL, NULL, NULL, NULL],
                                            ['backend.authitem.delete', 2, 'backend.authitem.delete', NULL, NULL, NULL, NULL],
                                            ['backend.authitem.index', 2, 'backend.authitem.index', NULL, NULL, NULL, NULL],
                                            ['backend.authitem.update', 2, 'backend.authitem.update', NULL, NULL, NULL, NULL],
                                            ['backend.authitem.view', 2, 'backend.authitem.view', NULL, NULL, NULL, NULL],
                                            ['backend.authitemchild.create', 2, 'backend.authitemchild.create', NULL, NULL, NULL, NULL],
                                            ['backend.authitemchild.delete', 2, 'backend.authitemchild.delete', NULL, NULL, NULL, NULL],
                                            ['backend.authitemchild.index', 2, 'backend.authitemchild.index', NULL, NULL, NULL, NULL],
                                            ['backend.authitemchild.update', 2, 'backend.authitemchild.update', NULL, NULL, NULL, NULL],
                                            ['backend.authitemchild.view', 2, 'backend.authitemchild.view', NULL, NULL, NULL, NULL],
                                            ['backend.censoredword.create', 2, 'backend.censoredword.index', NULL, '', NULL, NULL],
                                            ['backend.censoredword.delete', 2, 'backend.censoredword.index', NULL, '', NULL, NULL],
                                            ['backend.censoredword.index', 2, 'backend.censoredword.index', NULL, '', NULL, NULL],
                                            ['backend.censoredword.update', 2, 'backend.censoredword.index', NULL, '', NULL, NULL],
                                            ['backend.censoredword.view', 2, 'backend.censoredword.view', NULL, '', NULL, NULL],
                                            ['backend.users.create', 2, 'items', NULL, '', NULL, NULL],
                                            ['backend.users.delete', 2, 'items', NULL, '', NULL, NULL],
                                            ['backend.users.index', 2, 'backend.users.index', NULL, NULL, NULL, NULL],
                                            ['backend.users.update', 2, 'items', NULL, '', NULL, NULL],
                                            ['backend.users.view', 2, 'items', NULL, '', NULL, NULL],
                                            ['Frontend', 1, 'Frontend users can access only frontend actions', NULL, '', NULL, NULL],
                                            ['frontend.messages.create', 2, 'items', NULL, '', NULL, NULL],
                                            ['frontend.messages.delete', 2, 'items', NULL, '', NULL, NULL],
                                            ['frontend.messages.update', 2, 'items', NULL, '', NULL, NULL],
                                            ['frontend.messages.view', 2, 'items', NULL, '', NULL, NULL],
                                            ['frontend.site.save', 2, 'items', NULL, '', NULL, NULL],
                                            ['frontend.site.send', 2, 'items', NULL, '', NULL, NULL],
                                            ['frontend.site.user', 2, 'items', NULL, '', NULL, NULL],
                                            ['Super Admin', 1, 'Super Admin can create roles', NULL, '', NULL, NULL],]);
    }

    public function down()
    {
        echo "m161205_080922_rbac_migration_item cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
