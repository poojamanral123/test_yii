<?php

use yii\db\Migration;

/**
 * Handles the creation of table `censored_word`.
 */
class m161205_062611_create_censored_word_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%censored_word}}', [
           // 'id' => $this->primaryKey(),
            'name' => $this->string(30),
            'created_at' => $this->datetime(),
            'updated_at' => $this->timestamp(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%censored_word}}');
    }
}
