<?php

use yii\db\Migration;

class m161205_082522_rbac_migration_item_child extends Migration
{
    public function up()
    {
       $this->batchInsert('wb_auth_item_child', ['parent', 'child'],
                                    [['Super Admin', 'backend.authassignment.create'],
                                    ['Super Admin', 'backend.authassignment.delete'],
                                    ['Super Admin', 'backend.authassignment.index'],
                                    ['Super Admin', 'backend.authassignment.update'],
                                    ['Super Admin', 'backend.authitem.assign'],
                                    ['Super Admin', 'backend.authitem.create'],
                                    ['Super Admin', 'backend.authitem.delete'],
                                    ['Super Admin', 'backend.authitem.index'],
                                    ['Super Admin', 'backend.authitem.update'],
                                    ['Super Admin', 'backend.authitem.view'],
                                    ['Super Admin', 'backend.authitemchild.create'],
                                    ['Super Admin', 'backend.authitemchild.delete'],
                                    ['Super Admin', 'backend.authitemchild.index'],
                                    ['Super Admin', 'backend.authitemchild.update'],
                                    ['Super Admin', 'backend.authitemchild.view'],
                                    ['admin', 'backend.censoredword.create'],
                                    ['Super Admin', 'backend.censoredword.create'],
                                    ['admin', 'backend.censoredword.delete'],
                                    ['Super Admin', 'backend.censoredword.delete'],
                                    ['admin', 'backend.censoredword.index'],
                                    ['Super Admin', 'backend.censoredword.index'],
                                    ['admin', 'backend.censoredword.update'],
                                    ['Super Admin', 'backend.censoredword.update'],
                                    ['admin', 'backend.censoredword.view'],
                                    ['Super Admin', 'backend.censoredword.view'],
                                    ['admin', 'backend.users.create'],
                                    ['Super Admin', 'backend.users.create'],
                                    ['admin', 'backend.users.delete'],
                                    ['Super Admin', 'backend.users.delete'],
                                    ['admin', 'backend.users.index'],
                                    ['Super Admin', 'backend.users.index'],
                                    ['admin', 'backend.users.update'],
                                    ['Super Admin', 'backend.users.update'],
                                    ['admin', 'backend.users.view'],
                                    ['Super Admin', 'backend.users.view'],
                                    ['admin', 'frontend.messages.create'],
                                    ['Frontend', 'frontend.messages.create'],
                                    ['Super Admin', 'frontend.messages.create'],
                                    ['admin', 'frontend.messages.delete'],
                                    ['Frontend', 'frontend.messages.delete'],
                                    ['Super Admin', 'frontend.messages.delete'],
                                    ['admin', 'frontend.messages.update'],
                                    ['Frontend', 'frontend.messages.update'],
                                    ['Super Admin', 'frontend.messages.update'],
                                    ['admin', 'frontend.messages.view'],
                                    ['Frontend', 'frontend.messages.view'],
                                    ['Super Admin', 'frontend.messages.view'],
                                    ['admin', 'frontend.site.save'],
                                    ['Frontend', 'frontend.site.save'],
                                    ['Super Admin', 'frontend.site.save'],
                                    ['admin', 'frontend.site.send'],
                                    ['Frontend', 'frontend.site.send'],
                                    ['Super Admin', 'frontend.site.send'],
                                    ['admin', 'frontend.site.user'],
                                    ['Frontend', 'frontend.site.user'],
                                    ['Super Admin', 'frontend.site.user'],]);
    }

    public function down()
    {
        echo "m161205_080934_rbac_migration_itemchild cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp[]
    {
    }

    public function safeDown[]
    {
    }
    */
}
