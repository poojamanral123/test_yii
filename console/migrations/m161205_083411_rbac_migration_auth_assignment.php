<?php

use yii\db\Migration;

class m161205_083411_rbac_migration_auth_assignment extends Migration
{
    public function up()
    {

 $this->batchInsert('wb_auth_assignment', ['item_name','user_id','created_at'], [
                                          [ 'admin', '1',NULL],
                                          [ 'Frontend' ,'2',NULL],
                                          [ 'Super Admin' ,'3',NULL],
                                          ]);

    }

    public function down()
    {
        echo "m161205_080746_rbac_migration cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
