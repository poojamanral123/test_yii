<?php

use yii\db\Migration;

/**
 * Handles the creation of table `messages`.
 */
class m161205_063237_create_messages_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%messages}}', [
            'id' => $this->primaryKey(),
           // 'id' => $this->integer(),
            'from_user' => $this->string(30),
            'to_user' => $this->string(30),
            'message' => $this->string(15),
            'is_read' => $this->string(32),
            'created_at' => $this->datetime(),
            'updated_at' => $this->datetime(),
            'created_by' => $this->integer(),
            'modified_by' => $this->integer(),
        ]);
         $this->alterColumn('{{%messages}}', 'id', $this->smallInteger(8).' NOT NULL AUTO_INCREMENT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%messages}}');
    }
}
