<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
 use yii\helpers\ArrayHelper;
 use common\models\User;

/* @var $this yii\web\View */
/* @var $model frontend\models\Messages */
/* @var $form yii\widgets\ActiveForm */


$this->title = 'Create Messages';
$this->params['breadcrumbs'][] = ['label' => 'Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="messages-create">

    <h1><?= Html::encode($this->title) ;
        $user_name= Yii::$app->session['username'];
    ?></h1>

    <?php $form = ActiveForm::begin(['action' =>['save'], 'id' => 'forum_post', 'method' => 'post',]); ?>

<?php if($user_type=='Frontend'){?>
     <?= $form->field($model, 'to_user')->dropDownList(ArrayHelper::map(User::find()->where('username != :user and Type = :type', ['user'=>$user_name,'type'=>$user_type])->AsArray()->all(), 'username', 'first_name'), ['prompt' => '-Choose a user-']); ?>
<?php }else{?>
     <?= $form->field($model, 'to_user')->dropDownList(ArrayHelper::map(User::find()->where('username != :user ', ['user'=>$user_name])->AsArray()->all(), 'username', 'first_name'), ['prompt' => '-Choose a user-']); ?>
<?php } ?>
    <?= $form->field($model, 'message')->textInput() ?>

    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Send' : 'Send', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
  

    <?php ActiveForm::end(); ?>

    <h4 style ="color:green">
<?php  echo Yii::$app->getSession()->getFlash('success');?>
</h4>

</div>