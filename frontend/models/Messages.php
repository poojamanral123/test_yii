<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "wb_messages".
 *
 * @property integer $id
 * @property string $from_user
 * @property string $to_user
 * @property string $message
 * @property string $is_read
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $modified_by
 */
class Messages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wb_messages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'to_user', 'message'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'modified_by'], 'integer'],
            [['from_user', 'to_user'], 'string', 'max' => 50],
            [['is_read'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from_user' => 'From User',
            'to_user' => 'To User',
            'message' => 'Message',
            'is_read' => 'Is Read',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'modified_by' => 'Modified By',
        ];
    }
}
