<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "wb_censored_word".
 *
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property UserMessages[] $userMessages
 */
class CensoredWord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wb_censored_word';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserMessages()
    {
        return $this->hasMany(UserMessages::className(), ['rule_name' => 'name']);
    }
}
