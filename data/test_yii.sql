-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 02, 2016 at 07:57 PM
-- Server version: 5.5.53-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test_yii`
--

-- --------------------------------------------------------

--
-- Table structure for table `wb_auth_assignment`
--

CREATE TABLE IF NOT EXISTS `wb_auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wb_auth_assignment`
--

INSERT INTO `wb_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', '1', NULL),
('Frontend', '7', NULL),
('Super Admin', '8', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wb_auth_item`
--

CREATE TABLE IF NOT EXISTS `wb_auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wb_auth_item`
--

INSERT INTO `wb_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('admin', 1, 'Admin can perform backend actions', NULL, '', NULL, NULL),
('backend.authassignment.create', 2, 'backend.authassignment.create', NULL, '', NULL, NULL),
('backend.authassignment.delete', 2, 'sdasdas', NULL, '', NULL, NULL),
('backend.authassignment.index', 2, 'backend.authassignment.index', NULL, NULL, NULL, NULL),
('backend.authassignment.update', 2, 'sfsdf', NULL, '', NULL, NULL),
('backend.authitem.assign', 2, 'dasdsa', NULL, '', NULL, NULL),
('backend.authitem.create', 2, 'backend.authitem.create', NULL, NULL, NULL, NULL),
('backend.authitem.delete', 2, 'backend.authitem.delete', NULL, NULL, NULL, NULL),
('backend.authitem.index', 2, 'backend.authitem.index', NULL, NULL, NULL, NULL),
('backend.authitem.update', 2, 'backend.authitem.update', NULL, NULL, NULL, NULL),
('backend.authitem.view', 2, 'backend.authitem.view', NULL, NULL, NULL, NULL),
('backend.authitemchild.create', 2, 'backend.authitemchild.create', NULL, NULL, NULL, NULL),
('backend.authitemchild.delete', 2, 'backend.authitemchild.delete', NULL, NULL, NULL, NULL),
('backend.authitemchild.index', 2, 'backend.authitemchild.index', NULL, NULL, NULL, NULL),
('backend.authitemchild.update', 2, 'backend.authitemchild.update', NULL, NULL, NULL, NULL),
('backend.authitemchild.view', 2, 'backend.authitemchild.view', NULL, NULL, NULL, NULL),
('backend.censoredword.create', 2, 'backend.censoredword.index', NULL, '', NULL, NULL),
('backend.censoredword.delete', 2, 'backend.censoredword.index', NULL, '', NULL, NULL),
('backend.censoredword.index', 2, 'backend.censoredword.index', NULL, '', NULL, NULL),
('backend.censoredword.update', 2, 'backend.censoredword.index', NULL, '', NULL, NULL),
('backend.censoredword.view', 2, 'backend.censoredword.view', NULL, '', NULL, NULL),
('backend.users.create', 2, 'items', NULL, '', NULL, NULL),
('backend.users.delete', 2, 'items', NULL, '', NULL, NULL),
('backend.users.index', 2, 'backend.users.index', NULL, NULL, NULL, NULL),
('backend.users.update', 2, 'items', NULL, '', NULL, NULL),
('backend.users.view', 2, 'items', NULL, '', NULL, NULL),
('Frontend', 1, 'Frontend users can access only frontend actions', NULL, '', NULL, NULL),
('frontend.messages.create', 2, 'items', NULL, '', NULL, NULL),
('frontend.messages.delete', 2, 'items', NULL, '', NULL, NULL),
('frontend.messages.update', 2, 'items', NULL, '', NULL, NULL),
('frontend.messages.view', 2, 'items', NULL, '', NULL, NULL),
('frontend.site.save', 2, 'items', NULL, '', NULL, NULL),
('frontend.site.send', 2, 'items', NULL, '', NULL, NULL),
('frontend.site.user', 2, 'items', NULL, '', NULL, NULL),
('Super Admin', 1, 'Super Admin can create roles', NULL, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wb_auth_item_child`
--

CREATE TABLE IF NOT EXISTS `wb_auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wb_auth_item_child`
--

INSERT INTO `wb_auth_item_child` (`parent`, `child`) VALUES
('Super Admin', 'backend.authassignment.create'),
('Super Admin', 'backend.authassignment.delete'),
('Super Admin', 'backend.authassignment.index'),
('Super Admin', 'backend.authassignment.update'),
('Super Admin', 'backend.authitem.assign'),
('Super Admin', 'backend.authitem.create'),
('Super Admin', 'backend.authitem.delete'),
('Super Admin', 'backend.authitem.index'),
('Super Admin', 'backend.authitem.update'),
('Super Admin', 'backend.authitem.view'),
('Super Admin', 'backend.authitemchild.create'),
('Super Admin', 'backend.authitemchild.delete'),
('Super Admin', 'backend.authitemchild.index'),
('Super Admin', 'backend.authitemchild.update'),
('Super Admin', 'backend.authitemchild.view'),
('admin', 'backend.censoredword.create'),
('Super Admin', 'backend.censoredword.create'),
('admin', 'backend.censoredword.delete'),
('Super Admin', 'backend.censoredword.delete'),
('admin', 'backend.censoredword.index'),
('Super Admin', 'backend.censoredword.index'),
('admin', 'backend.censoredword.update'),
('Super Admin', 'backend.censoredword.update'),
('admin', 'backend.censoredword.view'),
('Super Admin', 'backend.censoredword.view'),
('admin', 'backend.users.create'),
('Super Admin', 'backend.users.create'),
('admin', 'backend.users.delete'),
('Super Admin', 'backend.users.delete'),
('admin', 'backend.users.index'),
('Super Admin', 'backend.users.index'),
('admin', 'backend.users.update'),
('Super Admin', 'backend.users.update'),
('admin', 'backend.users.view'),
('Super Admin', 'backend.users.view'),
('admin', 'frontend.messages.create'),
('Frontend', 'frontend.messages.create'),
('Super Admin', 'frontend.messages.create'),
('admin', 'frontend.messages.delete'),
('Frontend', 'frontend.messages.delete'),
('Super Admin', 'frontend.messages.delete'),
('admin', 'frontend.messages.update'),
('Frontend', 'frontend.messages.update'),
('Super Admin', 'frontend.messages.update'),
('admin', 'frontend.messages.view'),
('Frontend', 'frontend.messages.view'),
('Super Admin', 'frontend.messages.view'),
('admin', 'frontend.site.save'),
('Frontend', 'frontend.site.save'),
('Super Admin', 'frontend.site.save'),
('admin', 'frontend.site.send'),
('Frontend', 'frontend.site.send'),
('Super Admin', 'frontend.site.send'),
('admin', 'frontend.site.user'),
('Frontend', 'frontend.site.user'),
('Super Admin', 'frontend.site.user');

-- --------------------------------------------------------

--
-- Table structure for table `wb_auth_rule`
--

CREATE TABLE IF NOT EXISTS `wb_auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wb_censored_word`
--

CREATE TABLE IF NOT EXISTS `wb_censored_word` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wb_censored_word`
--

INSERT INTO `wb_censored_word` (`name`, `created_at`, `updated_at`) VALUES
('Badword', NULL, NULL),
('Badword2', NULL, NULL),
('Badword3', NULL, NULL),
('dfdgdf', NULL, NULL),
('hhhh', NULL, NULL),
('word11', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wb_messages`
--

CREATE TABLE IF NOT EXISTS `wb_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_user` varchar(50) NOT NULL,
  `to_user` varchar(50) NOT NULL,
  `message` varchar(300) NOT NULL,
  `is_read` varchar(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `wb_messages`
--

INSERT INTO `wb_messages` (`id`, `from_user`, `to_user`, `message`, `is_read`, `created_at`, `updated_at`, `created_by`, `modified_by`) VALUES
(1, '1', 'pp.mm', 'ddfgdf', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(2, '1', 'priya.123', 'helloo adasd Badword', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(3, '1', 'user1.user', 'werwer', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(4, '1', 'pp.mm', 'werw', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(5, '1', 'pp.mm', 'dsfsfsd', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(6, '1', 'pp.mm', 'Badword sdfsfsdfsf', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(7, '1', 'user1.user', 'tetert', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(8, '1', 'aaa.bbb', 'ssfsdfds', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wb_migration`
--

CREATE TABLE IF NOT EXISTS `wb_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wb_migration`
--

INSERT INTO `wb_migration` (`version`, `apply_time`) VALUES
('m140506_102106_rbac_init', 1480670455);

-- --------------------------------------------------------

--
-- Table structure for table `wb_users`
--

CREATE TABLE IF NOT EXISTS `wb_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `username` varchar(15) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(60) NOT NULL,
  `password_reset_token` varchar(100) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '2',
  `Type` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL COMMENT 'wb_user.id',
  `modified_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL COMMENT 'wb_user.id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `wb_users`
--

INSERT INTO `wb_users` (`id`, `first_name`, `last_name`, `username`, `auth_key`, `email`, `password`, `password_reset_token`, `status`, `Type`, `created_at`, `created_by`, `modified_at`, `modified_by`) VALUES
(1, 'Pooja', 'Manral', 'pooja.manral', '', 'pooja.manral@neosofttech.com', '$2y$13$P5oQH0KL9fXs2F4H34Iw/.yl1yfFzLy8pW951dXQQG.ImQBkvxaFu', '', 2, 'Admin', '2016-12-01 00:00:00', 1, '2016-12-02 09:46:59', 1),
(7, 'aaa', 'bbb', 'aaa.bbb', '', 'aabbb@gmail.com', '$2y$13$ujxlKPdr6IHNO1FSj68NPOYllbk53JwynUPQfiLJ4FBw7abbBdGYC', '', 2, 'Frontend', '2016-12-02 17:30:36', 1, '2016-12-02 12:00:36', NULL),
(8, 'Super', 'admin', 'super.admin', '', 'super@gmail.com', '$2y$13$ACM3/Zh9jrn2Y4H0SDyu1OGwmvd.1b6kuJxjIQ4DNAt1QGxA/u8.y', '', 2, 'Super Admin', '2016-12-02 19:10:56', 1, '2016-12-02 13:40:56', NULL),
(9, 'bbb.ccc', 'bbb', 'bbb.ccc', '', 'bbb@gmail.com', '$2y$13$v.9eJwfn3s3JRuitTyu.wul68YOd5IolTQ2MXkXtg.xT97A/MZ8zq', '', 2, 'Frontend', '2016-12-02 19:51:31', 1, '2016-12-02 14:21:31', NULL),
(10, 'pppp', 'user', 'pp.mm', '', 'pp@gmail.com', '$2y$13$zNCebty1RuCnuEAcQgV2G.LtF/5N.NfnoiC69akivo3mSvct1LIgW', '', 2, 'Frontend', '2016-12-02 19:52:18', 1, '2016-12-02 14:22:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wb_user_messages`
--

CREATE TABLE IF NOT EXISTS `wb_user_messages` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wb_user_messages`
--

INSERT INTO `wb_user_messages` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('color.create', 2, 'Item are authorized for add color ', NULL, '', NULL, NULL),
('color.delete', 2, 'Item are authorized for delete color', NULL, '', NULL, NULL),
('color.update', 2, 'Item are authorized for update color ', NULL, '', NULL, NULL),
('color.view', 2, 'Item are authorized for view color', NULL, '', NULL, NULL),
('reviewer', 1, 'Reviewer a person who can view all pages in project but not edit', NULL, 'Reviewer a person who can view all pages in project but not edit', NULL, NULL),
('typography.create', 2, 'Item are authorized for add typography ', NULL, '', NULL, NULL),
('typography.delete', 2, 'Item are authorized for delete typography', NULL, '', NULL, NULL),
('typography.update', 2, 'Item are authorized for update typography ', NULL, '', NULL, NULL),
('typography.view', 2, 'Item are authorized for view typography', NULL, '', NULL, NULL),
('users.create', 2, 'Item are authorized for add users ', NULL, '', NULL, NULL),
('users.delete', 2, 'Item are authorized for delete users', NULL, '', NULL, NULL),
('users.update', 2, 'Item are authorized for update users ', NULL, '', NULL, NULL),
('users.view', 2, 'Item are authorized for view users', NULL, '', NULL, NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `wb_auth_assignment`
--
ALTER TABLE `wb_auth_assignment`
  ADD CONSTRAINT `wb_auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `wb_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wb_auth_item`
--
ALTER TABLE `wb_auth_item`
  ADD CONSTRAINT `wb_auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `wb_auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `wb_auth_item_child`
--
ALTER TABLE `wb_auth_item_child`
  ADD CONSTRAINT `wb_auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `wb_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `wb_auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `wb_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wb_users`
--
ALTER TABLE `wb_users`
  ADD CONSTRAINT `wb_users_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `wb_users` (`id`),
  ADD CONSTRAINT `wb_users_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `wb_users` (`id`);

--
-- Constraints for table `wb_user_messages`
--
ALTER TABLE `wb_user_messages`
  ADD CONSTRAINT `wb_user_messages_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `wb_censored_word` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
