<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\CensoredWord */

$this->title = 'Create Censored Word';
$this->params['breadcrumbs'][] = ['label' => 'Censored Words', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="censored-word-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
