<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

 

/* @var $this yii\web\View */
/* @var $model app\models\AuthItemChild */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auth-item-child-form">  
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parent')->dropDownList(common\models\CommonUtility::getItems(1), ['prompt' => '-Choose a Roles-']); ?>
    <?= $form->field($model, 'child')->dropDownList(common\models\CommonUtility::getItems([2]), ['prompt' => '-Choose a Items-']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
