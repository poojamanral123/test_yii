<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AuthAssignmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Roles';
$this->params['breadcrumbs'][] = $this->title;
?>  
<?php
$this->registerCssFile(Yii::$app->request->baseUrl . "/../js/jstree/themes/default/style.min.css");
$this->registerJsFile(Yii::$app->request->baseUrl . "/../js/jstree/jstree.min.js", ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerJs("
    $('p#message').hide();
    $('#ajax').jstree({
        'checkbox': {
            'keep_selected_style': false
        },
        'plugins': ['checkbox'],
        'core': {
            'data': {
                'url': '" . Url::toRoute(['authitem/dataset']) . "',
                'dataType': 'json'
            }
        }
    });
    
    $( '#target' ).click(function() {
     
    var selectedElmsIds = [];
        var selectedElms = $('#ajax').jstree('get_selected', true);
            $.each(selectedElms, function() {                 
            selectedElmsIds.push(this.id);
            });
    var role_name = $( '#role_name' ).val();        
        if(role_name =='')
        {
        alert('Please select a role');
        return false;
        }
        $.ajax({
        url: '" . Url::toRoute(['authitem/setrolespermission']) . "',
        type: 'POST',
        data: { permissions: selectedElmsIds ,role_name: role_name},
        dataType: 'json',
        success: function (result) {
        $('p#message').show();
            setTimeout(function() { $('p#message').hide(); }, 4000);
        },
        error: function (xhr, ajaxOptions, thrownError) {
        //alert(xhr.status);

        }
    }); 
       
});    

", yii\web\View::POS_END);

$this->registerJs("
    $( '#role_name' ).change(function() {
        var role_name = $(this).val();
    
        $.ajax({
        url: '" . Url::toRoute(['authitem/getrolespermission']) . "',
        type: 'POST',
        data: { role_name: role_name},
        dataType: 'json',
        success: function (result) {
            $('#ajax').jstree(true).deselect_all();
            $('#ajax').jstree('close_all');
              if(result =='not found')
            {
            }else{
            $('#ajax').jstree(true).select_node(result); 
                }
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });        
});    

", yii\web\View::POS_END);
?>
<div class="auth-assignment-index">
     
    <p id='message' style="font-size: 25px; font-weight: 600; color: green;">Update role success fully.</p>
    <?=
    Html::dropDownList('role_name', '', common\models\CommonUtility::getItems(1), ['prompt' => '-Choose a Role-', 'class' => "form-control", 'id' => 'role_name'])
    ?>
    <div id="ajax" class="demo"></div>
    <?= Html::Button('Submit', ['class' => 'btn btn-primary', 'id' => 'target']) ?>
</div>