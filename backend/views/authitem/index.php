<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AuthItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Auth Item';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-item-index">
    <p>
        <?= Html::a('Create Auth Item', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'name',
                [
                'attribute' => 'type',
                'value' => function($searchModel) {
                    return ($searchModel->type == 1) ? "Roles" : "Items";
                },
                'filter' => [1 => "Roles", 2 => "Items"]
            ],
            'description:ntext',
//            'rule_name',
            'data:ntext',
            // 'created_at',
            // 'updated_at',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
</div>
