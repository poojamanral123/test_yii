<?php

namespace backend\controllers;

use Yii;
use common\models\AuthItem;
use common\models\AuthItemSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\CommonUtility;
use common\models\AuthItemChild;

/**
 * AuthitemController implements the CRUD actions for AuthItem model.
 */
class AuthitemController extends Controller {

    /**
     * @inheritdoc
     */
     public function behaviors() {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'create', 'update','delete','assign','view'],
                'rules' => [
                       [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['backend.authitem.index'],
                    ],
                        [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['backend.authitem.create'],
                    ],
                     [
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => ['backend.authitem.update'],
                    ],
                     [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['backend.authitem.delete'],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['backend.authitem.view'],
                    ],
                    [
                        'actions' => ['assign'],
                        'allow' => true,
                        'roles' => ['backend.authitem.assign'],
                    ],
                ],
            ],  
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'delete' => ['POST'],
//                ],
//            ],
        ];
    }

    /**
     * Lists all AuthItem models.
     * @return mixed
     */
    public function actionAssign() {
        return $this->render('jstree');
    }

    public function actionDataset() {
        $items = CommonUtility::getItems([2]);
        $tree_items = [];

        foreach ($items as $key => $value) {
            $item_explode = "";
            $item_explode = explode(".", $value);
            if (count($item_explode) == 2) {
                $tree_items[$item_explode[0]][] = $item_explode[1];
            } else if (count($item_explode) == 3) {
                $tree_items[$item_explode[0]][$item_explode[1]][] = $item_explode[2];
            }
        }

        $array_for_json = '';
        foreach ($tree_items as $key => $value) {
            $tree_json_key = '';
            $tree_json_key["id"] = $key;
            $tree_json_key["text"] = ucfirst($key);
            $i = 0;
            foreach ($value as $key_child => $value_child) {

                if (is_array($value_child)) {

                    foreach ($value_child as $key_sub_child => $key_sub_value) {
                        $tree_json_key["children"][$i]["id"] = $key . "." . $key_child;
                        $tree_json_key["children"][$i]["text"] = ucfirst($key_child);
                        $tree_json_key["children"][$i]["children"][$key_sub_child]["id"] = $key . "." . $key_child . "." . $key_sub_value;
                        $tree_json_key["children"][$i]["children"][$key_sub_child]["text"] = ucfirst($key_sub_value);
                    }
                } else {
                    $tree_json_key["children"][$i]["id"] = $key . "." . $value_child;
                    $tree_json_key["children"][$i]["text"] = ucfirst($value_child);
                }
                $i++;
            }
            $array_for_json[] = $tree_json_key;
        }
        echo json_encode($array_for_json);
    }

    public function actionGetrolespermission() {
        if (isset($_POST['role_name'])) {
            $role_name = $_POST['role_name'];
        }
        if ($role_name) {
            $child_items = CommonUtility::getItemsByRoles($role_name);
            echo json_encode($child_items);
        } else {
            echo json_encode(['not found']);
        }
    }

    public function actionSetrolespermission() {
        $role_name = '';
        if (isset($_POST['role_name'])) {
            $role_name = trim($_POST['role_name']);
        }
        if ($role_name) {
            $all_items = $valid_items = $role_items = $insert_items = $remove_items = [];

            if (isset($_POST['permissions'])) {
                $permissions = $_POST['permissions'];
            }

            $all_items = CommonUtility::getItems(2);
            $valid_items = array_intersect($all_items, $permissions);
            $role_items = CommonUtility::getItemsByRoles($role_name);
            $insert_items = array_diff($valid_items, $role_items);
            $remove_items = array_diff($role_items, $valid_items);
            if (!empty($insert_items)) {
                $AuthItemChildModel = new AuthItemChild();
                foreach ($insert_items as $value) {
                    $rows[] = ['parent' => $role_name, 'child' => $value];
                }
                Yii::$app->db->createCommand()->batchInsert(AuthItemChild::tableName(), $AuthItemChildModel->attributes(), $rows)->execute();
            }
            foreach ($remove_items as $value) {
                Yii::$app->db->createCommand()->delete(AuthItemChild::tableName(), 'parent=:parent AND child=:child', [':parent' => $role_name, 'child' => $value])->execute();
            }
            echo json_encode(['done']);
        }
    }

    public function actionIndex() {
        $searchModel = new AuthItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AuthItem model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AuthItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new AuthItem();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->name]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AuthItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->name]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AuthItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AuthItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AuthItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = AuthItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
