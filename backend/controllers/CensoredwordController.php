<?php

namespace backend\controllers;

use Yii;
use backend\models\CensoredWord;
use backend\models\CensoredWordSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CensoredwordController implements the CRUD actions for CensoredWord model.
 */
class CensoredwordController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'create', 'update','delete','view'],
                'rules' => [
                       [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['backend.censoredword.index'],
                    ],
                        [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['backend.censoredword.create'],
                    ],
                     [
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => ['backend.censoredword.update'],
                    ],
                     [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['backend.censoredword.delete'],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['backend.censoredword.view'],
                    ],
                ],
            ],  
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'delete' => ['POST'],
//                ],
//            ],
        ];
    }

    /**
     * Lists all CensoredWord models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CensoredWordSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CensoredWord model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CensoredWord model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CensoredWord();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CensoredWord model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CensoredWord model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CensoredWord model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CensoredWord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CensoredWord::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
